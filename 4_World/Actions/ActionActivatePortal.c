class ActionActivatePortal: ActionInteractBase
{
	PortalTrigger m_PortalTrigger = NULL;
	string m_PortalActionName;
	string m_PortalName;
	bool portalAvailable = false;
	bool m_UserAllowed = false;
	PlayerBase g_Player;
	/*
		#ifdef AMS_AdditionalMedicSupplies
		protected ref AMS_TestKitReportMenu m_Report1;
		#endif
	*/
	
	void ActionActivatePortal(ActionData action_data)
	{
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_INTERACTONCE;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		GetRPCManager().AddRPC("dzr_portals", "SetPortalAttributesOnClient", this, SingleplayerExecutionType.Client);
		GetRPCManager().AddRPC("dzr_portals", "SetUserAllowed", this, SingleplayerExecutionType.Client);
		//g_Player = action_data.m_Player;
	}
	
	
	void SetUserAllowed(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param1<bool> params;
		if (!ctx.Read(params))
		return;
		
		m_UserAllowed = params.param1;
	}
	
	
	void SetPortalAttributesOnClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param2<string,bool> params;
		if (!ctx.Read(params))
		return;
		
		m_PortalActionName = params.param1;
		Print(";;;;;;;;;;;;;;;;;;;;;;;;;;;; m_PortalActionName: "+params.param1);
		m_UserAllowed = params.param2;
		Print(";;;;;;;;;;;;;;;;;;;;;;;;;;;; m_UserAllowed: "+params.param2);
		Print(";;;;;;;;;;;;;;;;;;;;;;;;;;;; portalAvailable: "+portalAvailable);
		//DZR_sendPlayerMessage(sender, "SetPortalAttributesOnClient portalAvailable:"+portalAvailable+" m_UserAllowed:"+m_UserAllowed);
		
	}
	
	override void CreateConditionComponents() {
		m_ConditionItem = new CCINone;
        m_ConditionTarget = new CCTNone;
	}
	
	bool GetNearestPortal(PlayerBase player)
	{
		float distance;
		array<Object> nearestObjects = new array<Object>;
		vector pos = player.GetPosition();
		GetGame().GetObjectsAtPosition(pos, 1, nearestObjects, null);
		//m_PortalTrigger = NULL;
		for (int i = 0; i < nearestObjects.Count(); i++)
		{
			EntityAI nearestEnt = EntityAI.Cast(nearestObjects.Get(i));
			if (nearestEnt && nearestEnt.IsKindOf("PortalTrigger"))
			{
				
				m_PortalTrigger = PortalTrigger.Cast(nearestEnt);
				//m_PortalActionName = m_PortalTrigger.GetPortalActionName();
				//send RPC with portal. Server returns portal action name
				ref Param1<PortalTrigger> m_Data = new Param1<PortalTrigger>(m_PortalTrigger);
				GetRPCManager().SendRPC( "dzr_portals", "SendPortalAttributesToClient", m_Data, true, player.GetIdentity());
				//SetText(m_PortalActionName);
				portalAvailable = true;
			} 
			else
			{
				m_PortalTrigger = NULL;
				portalAvailable = false;
			}
		}
		DZR_sendPlayerMessage(player, "GetNearestPortal portalAvailable:"+portalAvailable+" m_UserAllowed:"+m_UserAllowed);
		return (portalAvailable && m_UserAllowed);
	}
	
	
	override string GetText()
	{
		return m_PortalActionName;
	}
	
	void SetText(string theActionName)
	{
		m_PortalActionName = theActionName;
		Print("Set Name for a portal: "+m_PortalActionName);
	}
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		return GetNearestPortal(player);		
	}
	
	void DZR_sendPlayerMessage(PlayerBase player, string message)	
	{
		Param1<string> Msgparam;
		Msgparam = new Param1<string>(message);
		GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam, true, player.GetIdentity());
	}
	
	void DZR_sendPlayerMessage2(PlayerBase player, string message)	
	{
		Param1<string> Msgparam;
		Msgparam = new Param1<string>(message);
		GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam, true, player.GetIdentity());
	}
	
    override void OnStart( ActionData action_data )
    {
        super.OnStart( action_data );
		NotificationSystem.SendNotificationToPlayerExtended(action_data.m_Player, 2, "OnStart Trying to teleport", "Trying to teleport", "set:dayz_gui image:open");
	}
	
	override void OnStartServer( ActionData action_data )
	{
		NotificationSystem.SendNotificationToPlayerExtended(action_data.m_Player, 2, "OnStartServer Trying to teleport", "Trying to teleport", "set:dayz_gui image:open");
		//PortalTrigger thePortal = PortalTrigger.Cast(m_PortalTrigger);
		/*
		bool allowTeleport;
		allowTeleport = true;
		if(m_PortalTrigger.GetisPrivate() == "1")
		{
			ref Param2<string, PortalTrigger> m_Data = new Param2<string, PortalTrigger>(action_data.m_Player.GetIdentity().GetPlainId(), m_PortalTrigger);
			GetRPCManager().SendRPC( "dzr_portals", "SendPlayerAllowed", m_Data, true, action_data.m_Player.GetIdentity());
			allowTeleport = m_UserAllowed;
		}
		//TODO: if private set bool, else set false bool
		// check bool 
		*/
		//if(allowTeleport)
		
			m_PortalName = m_PortalTrigger.GetPortalName();
			vector PortalDst = m_PortalTrigger.GetTarget().ToVector();
			
			vector PortalDstOri = Vector(0,0,0) + Vector(m_PortalTrigger.GetTargetOrientation().ToInt() , 0, 0) ;
			string PortalTitle = m_PortalTrigger.GetTitle();
			string PortalDesc1 = m_PortalTrigger.GetDescription1();
			//string m_PortalName = m_PortalTrigger.GetPortalName();
			string message = string.Format(PortalDesc1);
			//Print(message + ": " +m_PortalName);
			NotificationSystem.SendNotificationToPlayerExtended(action_data.m_Player, 2, m_PortalName, message, "set:dayz_gui image:open");
			
			action_data.m_Player.SetPosition(PortalDst);
			action_data.m_Player.SetOrientation(PortalDstOri);
			//string FileContent;
			//DZR_PTL_GetFile("$profile:DZR\\Portals\\"+m_PortalName,"ActionName.txt", m_PortalActionName, "");
			//m_PortalActionName = m_PortalTrigger.GetPortalActionName();
			//SetText(m_PortalActionName);
			//action_data.m_Player.SetLastUAMessage(m_PortalActionName);
			
			
			
			
			//Print("=========== $profile:DZR\\Portals\\"+m_PortalName+": ActionName.txt"+m_PortalActionName);
		
	}
};









