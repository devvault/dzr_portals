class PortalTrigger extends Inventory_Base
{
	
	string m_PortalActionName;
	string m_BlackScreenDelay;
	string m_BlackScreenWhileTeleporting;
	string m_Description1;
	string m_Description2;
	string m_Target;
	string m_PortalName;
	string m_isPrivate;
	string m_TargetOrientation;
	string m_PortalPosition;
	string m_Title;
	array<string> AllowedUsers;
	
	void PortalTrigger()
	{
		GetRPCManager().AddRPC( "dzr_portals", "SetPortalActionName", this, SingleplayerExecutionType.Both );
	}

	void SetPortalActionName2(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Print("Portal action name from server");
		ref Param1<string> data;
        if ( !ctx.Read( data ) )
		{
			return;	
		}
		
		
        if (type == CallType.Server)
        {
            if (data.param1)
            {
				//ref Param2<PlayerIdentity, string> m_Data = new Param2<PlayerIdentity, string>(data.param1, GetCharacterName());
				Print("Setting name: "+data.param1+" for "+m_PortalName);
				m_PortalActionName = data.param1;
				
			}
		}
	}

	bool DZR_PTL_GetFile(string folderPath, string fileName, out string fileContent, string defaults = "0", bool toDelete = false)
	{
		string m_TxtFileName = fileName;
		FileHandle fhandle;
		if ( FileExist(folderPath +"\\"+ m_TxtFileName) && !toDelete)
		{
			//file
			fhandle	=	OpenFile(folderPath +"\\"+ m_TxtFileName, FileMode.READ);
			//string server_id;
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			Print("[dzr_portals] :::  Config "+folderPath +"\\"+ m_TxtFileName+" is OK! Contents: "+fileContent);
			return true;
		}
		else 
		{
			
			TStringArray parts();
			string path = folderPath;
			path.Split("\\", parts);
			path = "";
			foreach (string part: parts)
			{
				path += part + "\\";
				if (part.IndexOf(":") == part.Length() - 1)
				continue;
				
				if(!toDelete)
				{
					if (!FileExist(path) && !MakeDirectory(path))
					{
						Print("Could not make dirs from path: " + path);
						return false;
					}
				}
				else
				{
					DeleteFile(path);
					Print("Deleted folder: "+path);
				}
			}
			
			string pth = folderPath +"\\"+ m_TxtFileName;
			FileHandle file = OpenFile(pth, FileMode.WRITE);
			
			if (file != 0 && !toDelete)
			{
				FPrintln(file, defaults);
				CloseFile(file);
				//file
				fhandle	=	OpenFile(folderPath +"\\"+ m_TxtFileName, FileMode.READ);
				//string server_id;
				FGets( file,  fileContent );
				
				Print("[dzr_portals] :::  Config "+folderPath +"\\"+ m_TxtFileName+" is OK! Contents: "+fileContent);
				CloseFile(file);
				
			}
		}
		Print("[dzr_portals] :::  Config "+folderPath +"\\"+ m_TxtFileName+" ERROR!");
		return false;
	}
	
	void AddAllowedUser(string SteamID)
	{
		AllowedUsers.Insert(SteamID);
	}
	
	//attribute
	string GetPortalActionName()
	{
		return m_PortalActionName;
	}
	
	void SetPortalActionName(string theValue)
	{
		m_PortalActionName = theValue;
		Print("[dzr_portals] :::  Set portal name "+theValue);
	}
	//attribute
	
	//attribute
	string GetBlackScreenDelay()
	{
		return m_BlackScreenDelay;
	}
	
	void SetBlackScreenDelay(string theValue)
	{
		m_BlackScreenDelay = theValue;
	}
	//attribute
	
	//attribute
	string GetBlackScreenWhileTeleporting()
	{
		return m_BlackScreenWhileTeleporting;
	}
	
	void SetBlackScreenWhileTeleporting(string theValue)
	{
		m_BlackScreenWhileTeleporting = theValue;
	}
	//attribute
	
	//attribute
	string GetDescription1()
	{
		return m_Description1;
	}
	
	void SetDescription1(string theValue)
	{
		m_Description1 = theValue;
	}
	//attribute
	
	//attribute
	string GetDescription2()
	{
		return m_Description2;
	}
	
	void SetDescription2(string theValue)
	{
		m_Description2 = theValue;
	}
	//attribute
	
	//attribute
	string GetTarget()
	{
		return m_Target;
	}
	
	void SetTarget(string theValue)
	{
		m_Target = theValue;
	}
	//attribute
	
	//attribute
	string GetPortalName()
	{
		return m_PortalName;
	}
	
	void SetPortalName(string theValue)
	{
		m_PortalName = theValue;
	}
	//attribute
	
	//attribute
	string GetisPrivate()
	{
		return m_isPrivate;
	}
	
	void SetisPrivate(string theValue)
	{
		m_isPrivate = theValue;
	}
	//attribute
	
	//attribute
	string GetTargetOrientation()
	{
		return m_TargetOrientation;
	}
	
	void SetTargetOrientation(string theValue)
	{
		m_TargetOrientation = theValue;
	}
	//attribute
	
	//attribute
	string GetPortalPosition()
	{
		return m_PortalPosition;
	}
	
	void SetPortalPosition(string theValue)
	{
		m_PortalPosition = theValue;
	}
	//attribute
	
	//attribute
	string GetTitle()
	{
		return m_Title;
	}
	
	void SetTitle(string theValue)
	{
		m_Title = theValue;
	}
	//attribute
	
	
	bool isPlayerAllowed(string PlainID)
	{
		if(FileExist( "$profile:DZR/Portals/"+m_PortalName+"/AllowedPlayers/" + PlainID ))
		{
			return true;	
		}
		return false;
	}
}