class CfgPatches
{
	class dzr_portals
	{
		requiredAddons[] = {"DZ_Data","DZ_Radio"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_portals
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "Admins can create public and private portals.";
		dir = "dzr_portals";
		name = "DZR Portals";
		//inputs = "dzr_portals/Data/Inputs.xml";
		dependencies[] = {"Core", "Game", "World", "Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_portals/Common",  "dzr_portals/1_Core"};
			};
			class gameLibScriptModule
			{
				files[] = {"dzr_portals/Common",  "dzr_portals/2_GameLib"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_portals/Common",  "dzr_portals/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_portals/Common", "dzr_portals/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_portals/Common", "dzr_portals/5_Mission"};
			};
			
		};
	};
};

class CfgVehicles
{
	class PersonalRadio;
	class PublicPortalDevice: PersonalRadio
	{
		displayName="Public Portal Creator";
		descriptionShort="Creates Public Portal";
		simulation="itemTransmitter";
		inputRange[]={};
		range=0;
		attachments[]=
		{
		};
		repairableWithKits[]={7};
		repairCosts[]={25};
		class EnergyManager
		{
		};
	};
	class PrivatePortalDevice: PublicPortalDevice
	{
		displayName="Private Portal Creator";
		descriptionShort="Creates Private Portal, adds the creator to it. Edited in the profile/DZR/DZR Portals folder";
	};
	class Inventory_Base;
	class PortalTrigger: Inventory_Base
	{
		scope = 1;
		storageCategory = 10;
		//model = "\dzr_portals\PortalTrigger.p3d";
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 100;
					healthLevels[] = {{1,{}},{0.7,{}},{0.5,{}},{0.3,{}},{0,{}}};
				};
			};
		};
		//hiddenSelections[] = {"camo"};
		//hiddenSelectionsTextures[] = {"\GarbageSearch\proxy\ProxySkin.paa"};
	};
};