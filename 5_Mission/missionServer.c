modded class MissionServer
{
	ref array<Object> m_Portals;
	ref map<string, Object> m_PortalObjects;
	int m_MsgDelay = 8;
	
	void MissionServer()
	{
		Print("[dzr_portals] ::: Starting Serverside");
		Print("[dzr_portals] ::: TODO: Load all portals from the folder");
		
		GetRPCManager().AddRPC("dzr_portals", "SendPortalAttributesToClient", this, SingleplayerExecutionType.Server);
		GetRPCManager().AddRPC("dzr_portals", "SendPlayerAllowed", this, SingleplayerExecutionType.Server);
		m_PortalObjects = new map<string, Object>;
		RecreatePortals();
	}
	
	void SendPlayerAllowed(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param2<string, PortalTrigger> params;
		if (!ctx.Read(params))
		return;
		
		ref Param1<bool> m_Data = new Param1<bool>(isPlayerAllowed(params.param1, params.param2.GetPortalName()));
		GetRPCManager().SendRPC( "dzr_portals", "SetUserAllowed", m_Data, true, sender);
	}
	
	bool isPlayerAllowed(string PlainID, string PortalName)
	{
		bool UserAllowed = true;
		Object thePortalObject;
		m_PortalObjects.Find(PortalName, thePortalObject);
		PortalTrigger thePortalTrigger = PortalTrigger.Cast(thePortalObject);
		if( (thePortalTrigger.GetisPrivate() == "1") && !FileExist( "$profile:DZR\\Portals\\"+PortalName+"\\AllowedPlayers\\" + PlainID +".txt" ) )
		{
			UserAllowed = false;	
			Print(PlainID +" ABSENT for portal "+PortalName);
		}
		return UserAllowed;
	}
	
	void SendPortalAttributesToClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param1<PortalTrigger> params;
		if (!ctx.Read(params))
		return;
		
		string ActionName = params.param1.GetPortalActionName();
		bool PlayerAllowed = isPlayerAllowed( sender.GetPlainId(), params.param1.GetPortalName());
		ref Param2<string, bool> m_Data = new Param2<string, bool>(ActionName, PlayerAllowed);
		GetRPCManager().SendRPC( "dzr_portals", "SetPortalAttributesOnClient", m_Data, true, sender);
		Print("SendPortalAttributesToClient ActionName:"+ActionName+" PlayerAllowed:"+PlayerAllowed);
	}
	
	bool RecreatePortals()
	{
		//portal array reset
		ClearPortals();
		//m_Portals = new array<PortalTrigger>; 
		//find all portal folders
		TStringArray PortalFolders = GetFoldersList("$profile:\\DZR\\Portals");
		m_Portals = CreatePortalsFromFolders(PortalFolders);
		Print(m_Portals);
		return true;
	}
	
	void ClearPortals()
	{
		if(m_Portals)
		{
			foreach(PortalTrigger a_Portal : m_Portals)
			{
				if(a_Portal)
				{
					GetGame().ObjectDelete( a_Portal );
					Print("======== Deleted ");
					Print(a_Portal);
				}
				else
				{
					Print( "-----------");
					Print( a_Portal);
					Print( "Does not exist");
					Print( "-----------");
				};
			}
		}
		else
		{
			"========= Portal array is NULL";
		}
	}
	
	array<Object> CreatePortalsFromFolders(TStringArray PortalFolders)
	{
		//foreach PortalFolders as PortalFolder
		
		if(!PortalFolders)
		{
			return NULL;
		};
		
		Object d_PortalTrigger;
		int index;
		array<Object> PortalsArray= new array<Object>;;
		if(m_PortalObjects.Count() != 0)
		{
			m_PortalObjects.Clear();
		};
		
		foreach (string PortalFolder : PortalFolders)
		{
			vector pos;
			vector ori;
			
			// Offset points for Garbage
			d_PortalTrigger = GetGame().CreateObject( "PortalTrigger", vector.Zero ); //GarbageDebug //GarbagePoint
			//vector z_newGBChernoOffsetA = z_GarbagePoint.GetPosition() + "0 50.95 0"; // 51.25 //.95 (1.13v)
			
			
			d_PortalTrigger.Update();
			index = PortalsArray.Insert(d_PortalTrigger);
			index = m_PortalObjects.Insert(PortalFolder, d_PortalTrigger);
			
			Print("New portal at index "+index.ToString() );
			
			PortalTrigger thePortalTrigger = PortalTrigger.Cast(d_PortalTrigger);
			Print("|||||||||||||||PORTAL OBJECT CREATED|||||||||||||||");
			Print(thePortalTrigger);
			Print("|||||||||||||||||||||||||||||||||||||||||||||||||||");
			//thePortalTrigger.SetPortalName(arg1);
			//thePortalTrigger.SetPrivate(arg2.ToInt());
			//d_PortalTrigger.SetSynchDirty();
			//d_PortalTrigger.SetLifetime(3888000);
			//AddPortalSrc(arg2, arg1, arg3);
			
			string FileContents;
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"ActionName.txt", FileContents);
			thePortalTrigger.SetPortalActionName(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"BlackScreenDelay.txt", FileContents);
			thePortalTrigger.SetBlackScreenDelay(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"BlackScreenWhileTeleporting.txt", FileContents);
			thePortalTrigger.SetBlackScreenWhileTeleporting(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"Description1.txt", FileContents);
			thePortalTrigger.SetDescription1(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"Description2.txt", FileContents);
			thePortalTrigger.SetDescription2(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"Target.txt", FileContents);
			thePortalTrigger.SetTarget(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"PortalName.txt", FileContents);
			thePortalTrigger.SetPortalName(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"isPrivate.txt", FileContents);
			thePortalTrigger.SetisPrivate(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"TargetOrientation.txt", FileContents);
			thePortalTrigger.SetTargetOrientation(FileContents);
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"PortalPosition.txt", FileContents);
			thePortalTrigger.SetPortalPosition(FileContents);
			d_PortalTrigger.SetPosition(FileContents.ToVector());
			//get portal attribute from file
			
			//get portal attribute from file
			DZR_PTL_GetFile("$profile:DZR\\Portals\\"+PortalFolder,"Title.txt", FileContents);
			thePortalTrigger.SetTitle(FileContents);
			//get portal attribute from file
			thePortalTrigger.Update();
			
		}		
		//foreach
		return PortalsArray;
	}
	
	
	private TStringArray GetFoldersList(string m_PathToFolders)
	{
		////Print("GetFoldersList");
		string	file_name;
		int file_attr;
		int		flags;
		TStringArray list = new TStringArray;
		
		
		
		string path_find_pattern = m_PathToFolders+"\\*"; //*/
		FindFileHandle file_handler = FindFile(path_find_pattern, file_name, file_attr, flags);
		
		bool found = true;
		while ( found )
		{				
			if ( file_attr == FileAttr.DIRECTORY )
			{
				////Print("GetFoldersList IS DIR: "+file_name);
				list.Insert(file_name);
			}
			
			found = FindNextFile(file_handler, file_name, file_attr);
		}
		
		return list;
	}
	
	bool DZR_PTL_GetFileLines(string folderPath, string fileName, out TStringArray fileContent)
	{
		string m_TxtFileName = fileName;
		FileHandle fhandle;
		fileContent = new TStringArray();
		
		if ( FileExist(folderPath +"\\"+ m_TxtFileName) )
		{
			fhandle	=	OpenFile(folderPath +"\\"+ m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				fileContent.Insert(line_content);
			}
			//Print("saved_access: "+saved_access);
			CloseFile(fhandle);				
			return true;
		}
		return false;
	}
	
	bool DZR_PTL_GetFile(string folderPath, string fileName, out string fileContent, string defaults = "0", bool toDelete = false)
	{
		string m_TxtFileName = fileName;
		FileHandle fhandle;
		if ( FileExist(folderPath +"\\"+ m_TxtFileName) && !toDelete)
		{
			//file
			fhandle	=	OpenFile(folderPath +"\\"+ m_TxtFileName, FileMode.READ);
			//string server_id;
			FGets( fhandle,  fileContent );
			CloseFile(fhandle);
			Print("[dzr_portals] :::  Config "+folderPath +"\\"+ m_TxtFileName+" is OK! Contents: "+fileContent);
			return true;
		}
		else 
		{
			
			TStringArray parts();
			string path = folderPath;
			path.Split("\\", parts);
			path = "";
			foreach (string part: parts)
			{
				path += part + "\\";
				if (part.IndexOf(":") == part.Length() - 1)
				continue;
				
				if(!toDelete)
				{
					if (!FileExist(path) && !MakeDirectory(path))
					{
						Print("Could not make dirs from path: " + path);
						return false;
					}
				}
				else
				{
					DeleteFile(path);
					Print("Deleted folder: "+path);
				}
			}
			
			string pth = folderPath +"\\"+ m_TxtFileName;
			FileHandle file = OpenFile(pth, FileMode.WRITE);
			
			if (file != 0 && !toDelete)
			{
				FPrintln(file, defaults);
				CloseFile(file);
				//file
				fhandle	=	OpenFile(folderPath +"\\"+ m_TxtFileName, FileMode.READ);
				//string server_id;
				FGets( file,  fileContent );
				
				Print("[dzr_portals] :::  Config "+folderPath +"\\"+ m_TxtFileName+" is OK! Contents: "+fileContent);
				CloseFile(file);
				return true;
			}
		}
		Print("[dzr_portals] :::  Config "+folderPath +"\\"+ m_TxtFileName+" ERROR!");
		return false;
	}
	
	void DZR_sendPlayerMessage(PlayerBase player, string message)	
	{
		Param1<string> Msgparam;
		Msgparam = new Param1<string>(message);
		GetGame().RPCSingleParam(player, ERPCs.RPC_USER_ACTION_MESSAGE, Msgparam, true, player.GetIdentity());
	}
	
	
	override void OnEvent(EventType eventTypeId, Param params) {
		super.OnEvent(eventTypeId,params);
		
		PlayerBase     player;
		PlayerIdentity identity;
		string null_content;
		
		switch(eventTypeId)
		{
			
			if(eventTypeId == ChatMessageEventTypeID ) {
				
				Print(params);
				
				ChatMessageEventParams chat_params = ChatMessageEventParams.Cast( params );
				
				array<Man> players = new array<Man>;
				GetGame().GetPlayers(players);
				for (int i = 0; i < players.Count(); ++i)
				{
					identity = PlayerBase.Cast(players.Get(i)).GetIdentity();
					if (identity.GetName() == chat_params.param2 || identity.GetPlainId() == chat_params.param2 || identity.GetPlayerId() == chat_params.param2.ToInt())
					{
						player = PlayerBase.Cast(players.Get(i));
					}
				}
				
				//ChatMessageEventParams chat_params = ChatMessageEventParams.Cast( params );
				//Class.CastTo(player, chat_params.param1);
				//identity = player.GetIdentity();
				//Print(chat_params.param1);
				//Print(chat_params.param2);
				//Print(chat_params.param3);
				//Print(chat_params.param4);
				//Print(chat_params.param5);
				//string message = chat_params.param3;
				
				//GetDayZGame().GetFelixBotAPI().newChatMessage(message, chat_params.param2);
				
				//identity = player.GetIdentity();
				
				if (chat_params.param3 != "") //trigger only when channel is Global == 0, Player Name does not equal to null and entered command
				{
					TStringArray args = new TStringArray();
					TStringArray portalAdmins = new TStringArray();
					chat_params.param3.Split(" ", args);
					string cmd = args[0];
					string arg1 = args[1];
					string arg2 = args[2];
					int arg3 = args[3].ToInt();
					string arg4 = args[4];
					
					// Read config
					string portalAdmin;
					bool IsAdmin = false;
					if ( DZR_PTL_GetFileLines("$profile:DZR\\Portals","PortalAdmins.txt", portalAdmins )
					{
				
						Print("portalAdmin: "+portalAdmin);
						//Print("arg4: "+arg4);
						if(identity)
						{
							if(portalAdmins.Find(identity.GetPlainId()) )
							{
								//addname 76561198000801794 Nommer 1 123
								//removename 76561198000801794 Nommer 1 123
								Print("[dzr_portals] Is admin: " + identity.GetPlainId());
								IsAdmin = true;
							}
							else
							{
								Print("[dzr_portals] Sorry, not admin: " + identity.GetPlainId());
							}
						}
					}
					// Read config
					if (IsAdmin)
					{
						vector pos;
						vector ori;
						
						switch(cmd) {
							// Add or Remove admin tag
							case "portalAdd":
						Print("adding portal source: "+arg1+" "+arg2);
						if(arg1 != "")
						{
							// Offset points for Garbage
							//Object d_PortalTrigger = GetGame().CreateObject( "PortalTrigger", vector.Zero ); //GarbageDebug //GarbagePoint
							//vector z_newGBChernoOffsetA = z_GarbagePoint.GetPosition() + "0 50.95 0"; // 51.25 //.95 (1.13v)
							pos = player.GetPosition(); // 51.25 //.95 (1.15v)
							ori = player.GetOrientation();
							
							/*
								d_PortalTrigger.SetPosition( pos );
								d_PortalTrigger.SetOrientation( ori );
								//this.AddChild( d_PortalTrigger, -1, false );
								d_PortalTrigger.Update();
								
								Print(d_PortalTrigger);
								
								PortalTrigger thePortalTrigger = PortalTrigger.Cast(d_PortalTrigger);
								Print(thePortalTrigger);
								thePortalTrigger.SetPortalName(arg1);
								thePortalTrigger.SetPrivate(arg2.ToInt());
								//d_PortalTrigger.SetSynchDirty();
								//d_PortalTrigger.SetLifetime(3888000);
								//AddPortalSrc(arg2, arg1, arg3);
							*/
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"ActionName.txt", null_content, "GO TO "+arg1);
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"BlackScreenDelay.txt", null_content, "10");
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"BlackScreenWhileTeleporting.txt", null_content, "1");
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"Description1.txt", null_content, "You have activated portal "+arg1);
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"Description2.txt", null_content, "Have a nice trip!");
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"Target.txt", null_content, "12918.8 64.9251 10004.7");
							
							//portal file
							string b_Private = arg2;
							if(arg2 == "")
							{ b_Private = "0"};
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"isPrivate.txt", null_content, b_Private);
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"TargetOrientation.txt", null_content, "-171 0 0");
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"PortalName.txt", null_content, arg1);
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"PortalPosition.txt", null_content, pos[0].ToString()+" "+pos[1].ToString()+" "+pos[2].ToString());
							
							//portal file
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"Title.txt", null_content, arg1);
							
							//add user
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1+"\\Users", identity.GetPlainId()+".txt", null_content, "1");
							
							//add private
							DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1+"\\AllowedPlayers", identity.GetPlainId()+".txt", null_content, "1");
							/*
								DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1, "isPrivate.txt", null_content, arg2);
								DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1, "isPrivate.txt", null_content, arg2);
								DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1, "Description.txt", null_content, "Portal descriptiotn is not set in profile/DZR/Portals/"+arg1+".");
								DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1, "BlackScreenWhileTeleporting.txt", null_content, "0");
								DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1, "BlackScreenDelay.txt", null_content, "8");
								DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1+"\\Users", identity.GetPlainId()+".txt", null_content, "1");
							*/
							NotificationSystem.SendNotificationToPlayerExtended(player, m_MsgDelay, "Portal "+arg1+" added", "Now set the Target by placing your character and typing portal_target "+arg1, "set:dayz_gui image:icon_flag");
							
							//ref Param1<string> m_Data = new Param1<string>("GO TO "+arg1);
							//GetRPCManager().SendRPC( "dzr_portals", "SetPortalActionName", m_Data, true, player.GetIdentity() );
							
						}
						else
						{
							NotificationSystem.SendNotificationToPlayerExtended(player, m_MsgDelay, "ERROR!", "Portal name cannot be empty. Use portal_add NAME>", "set:dayz_gui image:exit");
						}
						break;
						
						case "portalTarget":
						
						Print("adding portal Target: "+arg1+" "+arg2);
						pos = player.GetPosition(); // 51.25 //.95 (1.15v)
						ori = player.GetOrientation();
						
						
						if(FileExist("$profile:DZR\\Portals\\"+arg1+"\\Target.txt"))
						{
							DeleteFile("$profile:DZR\\Portals\\"+arg1+"\\Target.txt");
						}
						
						DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"Target.txt", null_content, pos[0].ToString()+" "+pos[1].ToString()+ " "+pos[2].ToString());
						
						
						if(FileExist("$profile:DZR\\Portals\\"+arg1+"\\TargetOrientation.txt"))
						{
							DeleteFile("$profile:DZR\\Portals\\"+arg1+"\\TargetOrientation.txt");
						}
						
						DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1,"TargetOrientation.txt", null_content, ori[0].ToString());
						NotificationSystem.SendNotificationToPlayerExtended(player, m_MsgDelay, "Portal "+arg1+" Target added", pos.ToString() + " "+ori.ToString()+"\r\nNow Open your profiles/DZR/Portals/"+arg1+" and configure your portal." , "set:dayz_gui image:settings");
						//AddPortalDest(arg2, arg1, arg3);
						RecreatePortals();
						
						
						break;
						
						case "portalAddUser":
						Print("adding portal user: "+arg1+" "+arg2);
						DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1+"\\Users",arg2+".txt", null_content, "1");
						break;
						
						case "portalRemoveUser":
						Print("removing portal user: "+arg1+" "+arg2);
						string m_Path = "$profile:DZR\\Portals\\"+arg1+"\\Users\\"+arg2+".txt";
						if(FileExist(m_Path)) {
							DeleteFile(m_Path);
						}
						break;
						
						case "portalRemove":
						Print("removing portal: "+arg1);
						DZR_PTL_GetFile("$profile:DZR\\Portals\\"+arg1, "", null_content, "0", true);
						//DeleteFile("$profile:DZR\\Portals\\"+arg1);
						//RemovePortal(arg2, arg1, arg3);
						break;
						
						case "portalsRefresh":
						Print("-------------------recreating portals");
						RecreatePortals();
						NotificationSystem.SendNotificationToPlayerExtended(player, m_MsgDelay, "DZR Portals+" , "Portals recreated", "set:dayz_gui image:settings");
						break;
						
						}
					}
					
				}
				
				
			}
		}
	}
}	
